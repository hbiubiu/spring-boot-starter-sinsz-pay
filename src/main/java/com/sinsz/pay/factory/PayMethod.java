package com.sinsz.pay.factory;

/**
 * 支付渠道类型
 * @author chenjianbo
 */
public enum PayMethod {
    /**
     * 微信app支付
     */
    WECHAT_APP,

    /**
     * 微信公众号支付
     */
    WECHAT_PUBLIC,

    /**
     * 支付宝手机网页支付
     */
    ALIPAY_WAP,

    /**
     * 支付宝app支付
     */
    ALIPAY_APP,

    /**
     * 银联
     */
    UNIONPAY

}
