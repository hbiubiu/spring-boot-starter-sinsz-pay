package com.sinsz.pay.support;

/**
 * 微信相关常量
 * @author chenjianbo
 */
public interface WxPayConstant extends Constant {

    /**
     * 支付接口版本
     */
    String VERSION = "1.0";

    /**
     * 设备信息
     */
    String DEVICE_INFO = "WEB";

    /**
     * 费用类型
     */
    String FEE_TYPE = "CNY";

    /**
     * 限制不能使用信用卡
     */
    String LIMIT_PAY = "no_credit";

    /**
     * 签名方式
     */
    String SIGN_TYPE = "MD5";

    /**
     * 压缩账单
     */
    String TAR_TYPE = "GZIP";

    /**
     * 退款资金来源
     * <p>
     *     REFUND_SOURCE_UNSETTLED_FUNDS---未结算资金退款（默认使用未结算资金退款）
     *     REFUND_SOURCE_RECHARGE_FUNDS---可用余额退款
     * </p>
     */
    String REFUND_ACCOUNT = "REFUND_SOURCE_UNSETTLED_FUNDS";

    /**
     * 微信打款强制校验真实姓名
     * <p>
     *     NO_CHECK：不校验真实姓名
     *     FORCE_CHECK：强校验真实姓名
     * </p>
     */
    String CHECK_NAME = "FORCE_CHECK";

    /**
     * 统一下单
     */
    String UNIFIEDORDER = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    /**
     * 查询订单
     */
    String ORDERQUERY = "https://api.mch.weixin.qq.com/pay/orderquery";

    /**
     * 关闭订单
     */
    String CLOSEORDER = "https://api.mch.weixin.qq.com/pay/closeorder";

    /**
     * 申请退款
     */
    String REFUND = "https://api.mch.weixin.qq.com/secapi/pay/refund";

    /**
     * 退款查询
     */
    String REFUNDQUERY = "https://api.mch.weixin.qq.com/pay/refundquery";

    /**
     * 下载对账单
     */
    String DOWNLOADBILL = "https://api.mch.weixin.qq.com/pay/downloadbill";

    /**
     * 企业付款
     */
    String TRANSFERS = "https://api.mch.weixin.qq.com/mmpaymkttransfers/promotion/transfers";

    /**
     * 查询企业付款
     */
    String TRANSFERSQUERY = "https://api.mch.weixin.qq.com/mmpaymkttransfers/gettransferinfo";

}
